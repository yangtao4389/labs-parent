package cn.angetech.javademo.lab02.handler;

import cn.angetech.javademo.lab02.message.AuthRequest;
import cn.angetech.javademo.lab02.message.AuthResponse;
import cn.angetech.javademo.lab02.message.UserJoinNoticeRequest;
import cn.angetech.javademo.lab02.util.WebSocketUtil;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.websocket.Session;

@Component
public class AuthMessageHandler implements MessageHandler<AuthRequest> {
    @Override
    public void execute(Session session, AuthRequest message) {
        // 如果未传递 accessToken
        if(StringUtils.isEmpty(message.getAccessToken())){
            WebSocketUtil.send(session, AuthRequest.TYPE, new AuthResponse().setCode(1).setMessage("认证accessToken 未传入"));
            return;
        }
        // 考虑到代码简化，我们先直接使用 accessToken 作为 User
        WebSocketUtil.addSession(session, message.getAccessToken());


        // 判断是否认证成功。这里，假装直接成功 返回认证成功字样
        WebSocketUtil.send(session, AuthResponse.TYPE, new AuthResponse().setCode(0));


        // 通知所有人，某个人加入了。这个是可选逻辑，仅仅是为了演示
        WebSocketUtil.broadcast(UserJoinNoticeRequest.TYPE,
                new UserJoinNoticeRequest().setNickname(message.getAccessToken())); // 考虑到代码简化，我们先直接使用 accessToken 作为 User

    }


    @Override
    public String getType() {
        return AuthRequest.TYPE;
    }
}
