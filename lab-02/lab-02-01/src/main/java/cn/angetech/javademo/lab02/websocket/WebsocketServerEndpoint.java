package cn.angetech.javademo.lab02.websocket;


import cn.angetech.javademo.lab02.message.AuthRequest;
import cn.angetech.javademo.lab02.message.Message;
import cn.angetech.javademo.lab02.util.WebSocketUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import cn.angetech.javademo.lab02.handler.MessageHandler;
import org.springframework.util.CollectionUtils;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

@Controller
@ServerEndpoint("/")
public class WebsocketServerEndpoint implements InitializingBean {
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 消息类型与 MessageHandler 的映射
     *
     * 注意，这里设置成静态变量。虽然说 WebsocketServerEndpoint 是单例，但是 Spring Boot 还是会为每个 WebSocket 创建一个 WebsocketServerEndpoint Bean 。
     */
    private static final Map<String, MessageHandler> HANDLERS = new HashMap<>();

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void afterPropertiesSet() throws Exception {
        // 通过 ApplicationContext 获得所有 MessageHandler Bean    避免手动配置 MessageHandler 与消息类型的映射。
        applicationContext.getBeansOfType(MessageHandler.class).values() // 获得所有 MessageHandler Bean
                .forEach(messageHandler -> HANDLERS.put(messageHandler.getType(), messageHandler)); // 添加到 handlers 中
        logger.info("[afterPropertiesSet][消息处理器数量：{}]", HANDLERS.size());
        logger.info("[afterPropertiesSet][消息处理器：{}]", HANDLERS);
    }

    @OnOpen
    public void onOpen(Session session, EndpointConfig config){
        logger.info("[onOpen][session({})接入]",session);
        // 实现连接时，使用 accessToken 参数进行用户认证  解析 ws:// 地址上的 accessToken 的请求参。例如说：ws://127.0.0.1:8080?accessToken=ddd
        List<String> accessTokenValues = session.getRequestParameterMap().get("accessToken");
        String accessToken = !CollectionUtils.isEmpty(accessTokenValues) ? accessTokenValues.get(0) : null;
        // 创建 AuthRequest 消息类型
        AuthRequest authRequest = new AuthRequest().setAccessToken(accessToken);
        // 获得消息处理器
        MessageHandler<AuthRequest> messageHandler = HANDLERS.get(AuthRequest.TYPE);
        if (messageHandler == null) {
            logger.error("[onOpen][认证消息类型，不存在消息处理器]");
            return;
        }
        messageHandler.execute(session, authRequest);
    }

    @OnMessage
    public void onMessage(Session session, String message){
        logger.info("[onMessage][session({})]接收到一条消息({})",session,message);
        try{
            // 获取消息类型
            JSONObject jsonObject = JSON.parseObject(message);
            String messageType = jsonObject.getString("type");
            // 获取消息处理器
            MessageHandler messageHandler = HANDLERS.get(messageType);
            if (messageHandler == null) {
                logger.error("[onMessage][消息类型({}) 不存在消息处理器]", messageType);
                return;
            }
            // 解析消息
            Class<? extends Message> messageClass = this.getMessageClass(messageHandler);
            Message messageObj = JSON.parseObject(jsonObject.getString("body"), messageClass);
            messageHandler.execute(session,messageObj);

        }catch (Throwable throwable){
            logger.info("[onMessage][session({}) message({}) 发生异常]", session, throwable);
        }

    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason){
        logger.info("[onClose][session({})] 连接关闭。原因是({})",session,closeReason);
        WebSocketUtil.removeSession(session);
    }

    @OnError
    public void onError(Session session,Throwable throwable){
        logger.info("[onError][session({})] 发生异常",session,throwable);
    }

    // 泛型处理  通过 messagehandler 来获取message 实现类
    private Class<? extends Message> getMessageClass(MessageHandler handler){
        // 获得 Bean 对应的 Class 类名。因为有可能被 AOP 代理过。
        Class<?> targetClass = AopProxyUtils.ultimateTargetClass(handler);
        // 获得接口的 Type 数组
        Type[] interfaces = targetClass.getGenericInterfaces();
        Class<?> superclass = targetClass.getSuperclass();
        // 此处，是以父类的接口为准
        while ( (Objects.isNull(interfaces) || 0 == interfaces.length) && Objects.nonNull(superclass) ){
            interfaces = superclass.getGenericInterfaces();
            superclass = targetClass.getSuperclass();
        }

        if(Objects.nonNull( interfaces)){
            // 遍历 interfaces 数组
            for (Type type:interfaces){
                // 要求 type 是泛型参数
                if (type instanceof ParameterizedType){
                    ParameterizedType parameterizedType = (ParameterizedType)type;
                    // 要求是 MessageHandler 接口
                    if( Objects.equals(parameterizedType.getRawType(),MessageHandler.class)){
                        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                        // 取首个元素
                        if(Objects.nonNull(actualTypeArguments) && actualTypeArguments.length>0){
                            return (Class<Message>)actualTypeArguments[0];
                        }else {
                            throw new IllegalStateException(String.format("类型(%s) 获得不到消息类型", handler));
                        }
                    }

                }
            }

        }

        throw new IllegalStateException(String.format("类型(%s) 获得不到消息类型", handler));
    }


}
