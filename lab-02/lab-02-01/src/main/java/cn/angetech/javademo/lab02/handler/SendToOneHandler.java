package cn.angetech.javademo.lab02.handler;

import cn.angetech.javademo.lab02.message.Message;
import cn.angetech.javademo.lab02.message.SendResponse;
import cn.angetech.javademo.lab02.message.SendToOneRequest;
import cn.angetech.javademo.lab02.message.SendToUserRequest;
import cn.angetech.javademo.lab02.util.WebSocketUtil;
import org.springframework.stereotype.Component;

import javax.websocket.Session;
@Component
public class SendToOneHandler implements MessageHandler<SendToOneRequest>{
    @Override
    public void execute(Session session, SendToOneRequest message) {
        //response 对应的message id 来自于 message
        SendResponse sendResponse = new SendResponse().setMsgId(message.getMsgId()).setCode(0);
        WebSocketUtil.send(session,SendResponse.TYPE,sendResponse);

        // 创建转发的消息
        SendToUserRequest sendToUserRequest = new SendToUserRequest().setMsgId(message.getMsgId()).setContent(message.getContent());
        WebSocketUtil.send(message.getToUser(), SendToUserRequest.TYPE, sendToUserRequest);

    }

    @Override
    public String getType() {
        return SendToOneRequest.TYPE;
    }
}
