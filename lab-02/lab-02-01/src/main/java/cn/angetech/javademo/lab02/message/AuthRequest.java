package cn.angetech.javademo.lab02.message;

public class AuthRequest implements Message {
    public static final String TYPE = "AUTH_REQUEST";

    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }

    public AuthRequest setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }
}
