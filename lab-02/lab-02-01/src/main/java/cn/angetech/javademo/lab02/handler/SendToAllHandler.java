package cn.angetech.javademo.lab02.handler;

import cn.angetech.javademo.lab02.message.Message;
import cn.angetech.javademo.lab02.message.SendResponse;
import cn.angetech.javademo.lab02.message.SendToAllRequest;
import cn.angetech.javademo.lab02.message.SendToUserRequest;
import cn.angetech.javademo.lab02.util.WebSocketUtil;
import org.springframework.stereotype.Component;

import javax.websocket.Session;
import java.util.WeakHashMap;

@Component
public class SendToAllHandler implements MessageHandler<SendToAllRequest> {
    @Override
    public void execute(Session session, SendToAllRequest message) {
        // 假装直接成功
        SendResponse sendResponse = new SendResponse().setMsgId(message.getMsgId()).setCode(0);
        WebSocketUtil.send(session,SendResponse.TYPE,sendResponse);

        // 创建转发的信息
        SendToUserRequest sendToUserRequest = new SendToUserRequest().setMsgId(message.getMsgId()).setContent(message.getContent());
        // 广播发送
        WebSocketUtil.broadcast(sendToUserRequest.TYPE, sendToUserRequest);
    }

    @Override
    public String getType() {
        return SendToAllRequest.TYPE;
    }
}
