package cn.angetech.javademo.lab03.yunaiserver.autoconfigure;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "yunai.server", ignoreUnknownFields = true)
public class YunaiServerProperties {
    // 默认端口
    private static final Integer DEFAULT_PORT = 8000;
    private Integer port = DEFAULT_PORT;

    public static Integer getDefaultPort() {
        return DEFAULT_PORT;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
