# 1.创建项目
通过idea中的maven构建web项目 lab-04-01

# 2. 添加maven依赖
```xml
  <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-core</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>${spring.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-web</artifactId>
            <version>${spring.version}</version>
        </dependency>

```
#3 配置web.xml
web.xml是web项目webapp/WEB-INF/web.xml的入口，在这里配置servlet的前端路由：DispatcherServlet，并在servlet-mapping 设置拦截路由
```xml
<servlet>
    <servlet-name>springmvc</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
    <!--    在启动的时候加载  -->
    <load-on-startup>1</load-on-startup> 
  </servlet>
  <servlet-mapping>
    <servlet-name>springmvc</servlet-name>
    <url-pattern>/</url-pattern>
  </servlet-mapping>
```
#4 在resources包里面定义springmvc.xml ，并在web.xml引入该配置
web.xml
```xml
 <servlet>
    <servlet-name>springmvc</servlet-name>
    <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>

    <init-param>
      <param-name>contextConfigLocation</param-name>
      <param-value>classpath:springmvc.xml</param-value>
    </init-param>
    <!--    在启动的时候加载  -->
    <load-on-startup>1</load-on-startup>
  </servlet>
  <servlet-mapping>
    <servlet-name>springmvc</servlet-name>
    <url-pattern>/</url-pattern>
  </servlet-mapping>

```
springmvc.xml中定义controller扫描的类以及jsp文件放的位置，在webapp里面先建一个pages目录
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">


    <context:component-scan base-package="cn.angetech.javademo.lab04.controller"/>

    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="prefix">
            <value>/pages/</value>
        </property>
        <property name="suffix">
            <value>.jsp</value>
        </property>
    </bean>
</beans>
```
并在java目录下生成该controller，实现HelloController，返回helloword
```java
@Controller
public class HelloController {
    @RequestMapping("/hello")
    public String sayHello(){
        return "hello";
    }
}
```
controller返回的hello，会被映射到webapp/pages/hello.jsp
定义hello.jsp
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>这里是欢迎页面</h1>

</body>
</html>
```
#5 tomcat启动
配置tomcat，配置lab_04_01_war_exploded包，并启动该项目lab-04-01

访问：http://localhost:8080/lab_04_01_war_exploded/hello，则得到我们定义的 欢迎页面  

初步helloword已经完成


#6 controller详解
[按照官方文档使用配置以及controller](https://docs.spring.io/spring/docs/4.3.28.RELEASE/spring-framework-reference/htmlsingle/#mvc)