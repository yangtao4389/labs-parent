> 说明：
> 
> 该项目涵盖了平时用过的代码、注释，非常适合快速找到需要的代码及逻辑
> 主要通过注释 pom.xml里面的注解，来实现运行不同的项目

## 基础

|内容|模块|说明|
|----|----|----|
|springboot+mvc|lab-01|入门教程|
|springboot+websocket|lab-02|入门教程|
|springboot 自动配置原理 |lab-03|原理|
|springmvc |lab-04|入门教程|

