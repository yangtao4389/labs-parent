原创出处：<http://www.iocoder.cn/Spring-Boot/SpringMVC/?github>

# 1.概述
Java Web早期，是 Struts2 与 SpringMVC 双雄争霸的年代。

现在都统一使用SpringMVC，对于其处理请求的整体流程，一定要倒背如流。

# 2.快速入门
## 2.1.引入依赖
在**pom.xml**文件中，我们实现spring-boot集成的web配置
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.3.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>lab-01</artifactId>

    <dependencies>
        <!-- 实现对 Spring MVC 的自动化配置 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <!-- 方便等会写单元测试 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

</project>
```
## 2.2.Application
创建**Application**类
```java

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }
}
```
此时可以直接运行，会有一个8080端口的web项目启动。
## 2.3.配置controller
Controller负责接收url请求并处理，我们根据restful设计准则，定义一个用户的增删改查接口。

|请求方法| URL  | 功能 |
| :----| :---- | :---- |
| GET | /users | 查询用户列表 |
| GET | /users/{id} | 查询指定用户编号的用户 |
| POST | /users | 添加用户 |
| PUT | /users/{id} | 更新指定用户编号的用户 |
| DELETE | /users/{id} | 删除指定用户编号的用户 |

创建UserController类，代码如下
```java
import cn.angetech.javademo.lab01.dto.UserAddDTO;
import cn.angetech.javademo.lab01.dto.UserUpdateDTO;
import cn.angetech.javademo.lab01.vo.UserVO;
import org.springframework.web.bind.annotation.*;

import javax.swing.plaf.PanelUI;
import javax.swing.text.StyledEditorKit;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @GetMapping("")
    public List<UserVO> list(){
        ArrayList<UserVO> result = new ArrayList<>();
        result.add(new UserVO().setId(1).setUsername("zhangsan"));
        result.add(new UserVO().setId(2).setUsername("lisi"));
        result.add(new UserVO().setId(3).setUsername("wanger"));
        return result;

    }

    @GetMapping("/{id}")
    public UserVO get(@PathVariable("id") Integer id){
        return new UserVO().setId(id).setUsername("username:"+id);
    }

    @PostMapping("")
    public Integer add(UserAddDTO addDTO){
        // 返回用户编号
        return 1;
    }

    @PutMapping("/{id}")
    public Boolean update(@PathVariable("id") Integer id, UserUpdateDTO userUpdateDTO){
        userUpdateDTO.setId(id);
        Boolean success = true;
        return success;
    }

    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable("id") Integer id){
        return false;
    }

}
```
通过postman请求上述定义好的接口，得到相应返回结果。
## 2.4.解释几个注释
### 2.4.1.@Controller 注解
添加在类上，表示这是控制器 Controller 对象。属性如下：

> name 属性：该 Controller 对象的 Bean 名字。允许空。

@RestController 注解，添加在类上，是 @Controller 和 @ResponseBody 的组合注解，直接使用接口方法的返回结果，经过 JSON/XML 等序列化方式，最终返回。也就是说，无需使用 InternalResourceViewResolver 解析视图，返回 HTML 结果。  

目前主流的架构，都是 前后端分离 的架构，后端只需要提供 API 接口，仅仅返回数据。而视图部分的工作，全部交给前端来做。也因此，我们项目中 99.99% 使用 @RestController 注解。

### 2.4.2.@RequestMapping 注解
添加在类或方法上，标记该类/方法对应接口的配置信息。

@RequestMapping 注解的常用属性，如下：

>path 属性：接口路径。[] 数组，可以填写多个接口路径。

>values 属性：和 path 属性相同，是它的别名。

>method 属性：请求方法 RequestMethod ，可以填写 GET、POST、POST、DELETE 等等。[] 数组，可以填写多个请求方法。如果为空，表示匹配所有请求方法。

@RequestMapping 注解的不常用属性，如下：

>name 属性：接口名。一般情况下，我们不填写。

>params 属性：请求参数需要包含值的参数名。可以填写多个参数名。如果为空，表示匹配所有请你求方法。

>headers 属性：和 params 类似，只是从参数名变成请求头。

>consumes 属性：和 params 类似，只是从参数名变成请求头的提交内容类型( Content-Type )

>produces 属性：和 params 类似，只是从参数名变成请求头的( Accept )可接受类型。

考虑到让开发更加方便，Spring 给每种请求方法提供了对应的注解：

>@GetMapping 注解：对应 @GET 请求方法的 @RequestMapping 注解。

>@PostMapping 注解：对应 @POST 请求方法的 @RequestMapping 注解。

>@PutMapping 注解：对应 @PUT 请求方法的 @RequestMapping 注解。

>@DeleteMapping 注解：对应 @DELETE 请求方法的 @RequestMapping 注解。

>还有其它几个，就不一一列举了

### 2.4.3.@RequestParam
@RequestParam 注解，添加在方法参数上，标记该方法参数对应的请求参数的信息。属性如下：

>name 属性：对应的请求参数名。如果为空，则直接使用方法上的参数变量名。

>value 属性：和 name 属性相同，是它的别名。

>required 属性：参数是否必须传。默认为 true ，表示必传。

>defaultValue 属性：参数默认值。

@PathVariable 注解，添加在方法参数上，标记接口路径和方法参数的映射关系。具体的，我们在示例中来看。相比 @RequestParam 注解，少一个 defaultValue 属性。

## 2.5.类Restful API

|请求方法| URL  | 功能 |
| :----| :---- | :---- |
| GET | /users2/list | 查询用户列表 |
| GET | /users2/get | 查询指定用户编号的用户 |
| POST | /users2/add | 添加用户 |
| PUT | /users2/update | 更新指定用户编号的用户 |
| DELETE | /users2/delete | 删除指定用户编号的用户 |

具体代码：
```java
import cn.angetech.javademo.lab01.dto.UserAddDTO;
import cn.angetech.javademo.lab01.dto.UserUpdateDTO;
import cn.angetech.javademo.lab01.vo.UserVO;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users2")
public class UserContoller2 {

    @GetMapping("/list")  // URL修改为 /list
    public List<UserVO> list(){
        ArrayList<UserVO> result = new ArrayList<>();
        result.add(new UserVO().setId(1).setUsername("zhangsan"));
        result.add(new UserVO().setId(2).setUsername("lisi"));
        result.add(new UserVO().setId(3).setUsername("wanger"));
        return result;
    }

    @GetMapping("/get") // URL 修改成 /get  并将/id 变为?id=  param传入
    public UserVO get(@RequestParam("id") Integer id){
        return new UserVO().setId(id).setUsername("username:"+id);
    }

    @PostMapping("/add")
    public Integer add(UserAddDTO addDTO){
        // 返回用户编号
        return 1;
    }

    @PutMapping("/update")
    public Boolean update( UserUpdateDTO userUpdateDTO){
        return true;
    }


    @DeleteMapping("/delete")
    public Boolean delete(@RequestParam("id") Integer id){
        return false;
    }
}

```
由于平时的业务比较复杂，建议用这种类Restful接口。
另外： SpringMVC 提供的 @PathVariable 路径参数可能面对一些问题不太适合使用，比如：
>1、封装的权限框架，基于 URL (get/add/update/delete)作为权限标识，暂时是不支持带有路径参数的 URL 。

>2、基于 URL 进行告警，而带有路径参数的 URL ，“相同” URL 实际对应的是不同的 URL ，导致无法很方便的实现按照单位时间请求错误次数告警。

>3、@PathVariable 路径参数的 URL ，会带来一定的 SpringMVC 的性能下滑。具体可以看看 《SpringMVC RESTful 性能优化》 文章

# 3.测试接口
