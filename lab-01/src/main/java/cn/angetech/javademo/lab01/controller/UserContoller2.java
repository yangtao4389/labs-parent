package cn.angetech.javademo.lab01.controller;

import cn.angetech.javademo.lab01.dto.UserAddDTO;
import cn.angetech.javademo.lab01.dto.UserUpdateDTO;
import cn.angetech.javademo.lab01.vo.UserVO;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users2")
public class UserContoller2 {

    @GetMapping("/list")  // URL修改为 /list
    public List<UserVO> list(){
        ArrayList<UserVO> result = new ArrayList<>();
        result.add(new UserVO().setId(1).setUsername("zhangsan"));
        result.add(new UserVO().setId(2).setUsername("lisi"));
        result.add(new UserVO().setId(3).setUsername("wanger"));
        return result;
    }

    @GetMapping("/get") // URL 修改成 /get  并将/id 变为?id=  param传入
    public UserVO get(@RequestParam("id") Integer id){
        return new UserVO().setId(id).setUsername("username:"+id);
    }

    @PostMapping("/add")
    public Integer add(UserAddDTO addDTO){
        // 返回用户编号
        return 1;
    }

    @PutMapping("/update")
    public Boolean update( UserUpdateDTO userUpdateDTO){
        return true;
    }


    @DeleteMapping("/delete")
    public Boolean delete(@RequestParam("id") Integer id){
        return false;
    }
}
