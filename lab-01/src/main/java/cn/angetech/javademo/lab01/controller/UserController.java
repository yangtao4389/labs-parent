package cn.angetech.javademo.lab01.controller;


import cn.angetech.javademo.lab01.dto.UserAddDTO;
import cn.angetech.javademo.lab01.dto.UserUpdateDTO;
import cn.angetech.javademo.lab01.vo.UserVO;
import org.springframework.web.bind.annotation.*;

import javax.swing.plaf.PanelUI;
import javax.swing.text.StyledEditorKit;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @GetMapping("")
    public List<UserVO> list(){
        ArrayList<UserVO> result = new ArrayList<>();
        result.add(new UserVO().setId(1).setUsername("zhangsan"));
        result.add(new UserVO().setId(2).setUsername("lisi"));
        result.add(new UserVO().setId(3).setUsername("wanger"));
        return result;

    }

    @GetMapping("/{id}")
    public UserVO get(@PathVariable("id") Integer id){
        return new UserVO().setId(id).setUsername("username:"+id);
    }

    @PostMapping("")
    public Integer add(UserAddDTO addDTO){
        // 返回用户编号
        return 1;
    }

    @PutMapping("/{id}")
    public Boolean update(@PathVariable("id") Integer id, UserUpdateDTO userUpdateDTO){
        userUpdateDTO.setId(id);
        Boolean success = true;
        return success;
    }

    @DeleteMapping("/{id}")
    public Boolean delete(@PathVariable("id") Integer id){
        return false;
    }


}
